

'use strict';
const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');

const app = express();


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.set('appName', 'patent');

app.use(bodyParser.json());

// Define your own router file in controller folder, export the router, add it into the index.js.

app.use('/', require('./controller/restapi/router'));

let server = http.createServer();



// now set up the http server
server.on( 'request', app );
server.listen('2000', function() {console.log('Listening locally on port %d', server.address().port);});

