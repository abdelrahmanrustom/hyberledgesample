/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';

let express = require('express');
let router = express.Router();

let hlcAdmin = require('./fabric/hlcAdmin');
let hlcClient = require('./fabric/hlcClient');
let setup = require('./fabric/autoLoad');
let blockchain = require('./fabric/getBlockchain');

router.post('/setup/autoLoad*', setup.autoLoad);

module.exports = router;


router.post('/fabric/getBlockchain', blockchain.getBlockchain);

router.get('/fabric/admin/getRegistries', hlcAdmin.getRegistries);
router.post('/fabric/admin/getMembers', hlcAdmin.getMembers);
router.post('/fabric/admin/getAssets', hlcAdmin.getAssets);
router.post('/fabric/admin/addMember', hlcAdmin.addMember);

// router requests specific to the participant
// router.get('/fabric/client/getItemTable', hlcClient.getItemTable);
// router.post('/fabric/client/getMyOrders', hlcClient.getMyOrders);
router.post('/fabric/client/addPatent', hlcClient.addPatent);
router.post('/fabric/client/patentAction', hlcClient.patentAction);