/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


'use strict';
let fs = require('fs');
let path = require('path');

// let itemTable = null;
// const svc = require('./Z2B_Services');
// const financeCoID = 'easymoney@easymoneyinc.com';

// Bring Fabric SDK network class
const { FileSystemWallet, Gateway } = require('fabric-network');

// A wallet stores a collection of identities for use
let walletDir = path.join(path.dirname(require.main.filename),'controller/restapi/fabric/_idwallet');
const wallet = new FileSystemWallet(walletDir);

const ccpPath = path.resolve(__dirname, 'connection.json');
const ccpJSON = fs.readFileSync(ccpPath, 'utf8');
const ccp = JSON.parse(ccpJSON);


/**
 * patentAction - act on an order for a buyer
 * @param {express.req} req - the inbound request object from the client
 * req.body.action - string with requested action
 * available actions are:
 * Default  - Newly created patent
 * Pending - publish created patent requires a verifier_id, publisher_id
 * Published - publish created patent requires publisher_id
 * Rejected - Rejected an existing patent. requires a reason, verifier_id, publisher_id
 * req.body.participant - string with buyer id
 * req.body.patentNo - string with patentNo to be acted upon
 * req.body.reason - reason for reject, required for reject processing to proceed
 * @param {express.res} res - the outbound response object for communicating back to client
 * @param {express.next} next - an express service to enable post processing prior to responding to the client
 * @returns {Object} patent
 * @function
 */
exports.patentAction = async function (req, res, next) {
    //let method = 'patentAction';
    
    // if ((req.body.action === 'Dispute') && (typeof(req.body.reason) !== 'undefined') && (req.body.reason.length > 0) )
    // {/*let reason = req.body.reason;*/}
    // else {
    //     if ((req.body.action === 'Dispute') && ((typeof(req.body.reason) === 'undefined') || (req.body.reason.length <1) ))
    //         {res.send({'result': 'failed', 'error': 'no reason provided for dispute'});}
    // }

    // Main try/catch block
    try {

        // A gateway defines the peers used to access Fabric networks
        const gateway = new Gateway();
        await gateway.connect(ccp, { wallet, identity: 'User1@org1.example.com', discovery: { enabled: false } });

        // Get addressability to network
        const network = await gateway.getNetwork('mychannel');

        // Get addressability to  contract
        const contract = await network.getContract('patentcontract');


        // Get state of patent
        const responsePatent = await contract.evaluateTransaction('query', req.body.patentNumber);
        console.log('responseOrder: ');
        console.log(JSON.parse(responsePatent.toString()));
        let patent = JSON.parse(JSON.parse(responsePatent.toString()));
        
        // Perform action on the order
        switch (req.body.action)
        {
        case 'Pending':
            console.log('Pending entered');
            const pendingPatentResponse = await contract.submitTransaction('VerifyPatent', patent.patentNumber, req.body.verifierId, req.body.publisherId, req.body.action, '');
            console.log('pendingPatentResponse: ');
            console.log(JSON.parse(pendingPatentResponse.toString()));
            break;
        case 'Reject':
            console.log('Reject entered');
            const rejectPatentResponse = await contract.submitTransaction('VerifyPatent', patent.patentNumber, req.body.verifierId, req.body.publisherId, req.body.action, req.body.reason);
            console.log('rejectPatentResponse: ');
            console.log(JSON.parse(rejectPatentResponse.toString()));          
            break;
        case 'Publish':
            console.log('Publish entered');
            const publishPatentResponse = await contract.submitTransaction('PublishPatent', patent.patentNumber, req.body.publisherId);
            console.log('publishPatentResponse: ');
            console.log(JSON.parse(publishPatentResponse.toString()));
            break;
        default :
            console.log('default entered for action: '+req.body.action);
            res.send({'result': 'failed', 'error':' order '+req.body.orderNo+' unrecognized request: '+req.body.action});
        }
        
        // Disconnect from the gateway
        console.log('Disconnect from Fabric gateway.');
        console.log('patentAction Complete');
        await gateway.disconnect();
        res.send({'result': ' patent '+req.body.patentNumber+' successfully updated to '+req.body.action});
            
    } catch (error) {
        console.log(`Error processing transaction. ${error}`);
        console.log(error.stack);
        res.send({'error': error.stack});
    } 

};

/**
 * adds a patent to the blockchain
 * @param {express.req} req - the inbound request object from the client
 * req.body.ownerId - string with owner id
 * req.body.verifierId - string with verifier id
 * req.body.ownersId - array with owners id
 * req.body.patentIndustry - string with new patentIndustry
 * req.body.priorArt - string with new priorArt
 * req.body.details - string with new details
 * @param {express.res} res - the outbound response object for communicating back to client
 * @param {express.next} next - an express service to enable post processing prior to responding to the client
 * @returns {Array} an array of assets
 * @function
 */
exports.addPatent = async function (req, res, next) {
    let method = 'addPatent';
    console.log(method+' req.body.owner is: '+req.body.owner );    
    let patentNumber = '00' + Math.floor(Math.random() * 10000);
    // Main try/catch block
    try {

        // A gateway defines the peers used to access Fabric networks
        const gateway = new Gateway();
        await gateway.connect(ccp, { wallet, identity: 'User1@org1.example.com', discovery: { enabled: false } });

        // Get addressability to network
        const network = await gateway.getNetwork('mychannel');

        // Get addressability to  contract
        const contract = await network.getContract('patentcontract');

        const createPatentResponse = await contract.submitTransaction(
            'CreatePatentRequest', 
            req.body.ownerId, 
            req.body.verifierId,
            req.body.ownersId,
            patentNumber, 
            req.body.patentIndustry,
            req.body.priorArt,
            req.body.details);
        console.log('createPatentResponse: ')
        console.log(JSON.parse(createPatentResponse.toString()));

        // Disconnect from the gateway
        console.log('Disconnect from Fabric gateway.');
        console.log('addPatent Complete');
        await gateway.disconnect();
        res.send({'result': ' patent '+patentNumber+' successfully added'});

    } catch (error) {
        console.log(`Error processing transaction. ${error}`);
        console.log(error.stack);
        res.send({'error': error.stack});
    } 
    
};



