/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const MyContract = require('./lib/patentContract');

module.exports.contracts = [ MyContract ];
