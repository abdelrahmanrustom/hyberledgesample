#!/bin/bash

echo
echo " ____    _____      _      ____    _____ "
echo "/ ___|  |_   _|    / \    |  _ \  |_   _|"
echo "\___ \    | |     / _ \   | |_) |   | |  "
echo " ___) |   | |    / ___ \  |  _ <    | |  "
echo "|____/    |_|   /_/   \_\ |_| \_\   |_|  "
echo
echo "Build your first network (BYFN) end-to-end test"
echo
CHANNEL_NAME="$1"
DELAY="$2"
LANGUAGE="$3"
TIMEOUT="$4"
VERBOSE="$5"
: ${CHANNEL_NAME:="firstchannel"}
: ${DELAY:="3"}
: ${LANGUAGE:="node"}
: ${TIMEOUT:="10"}
: ${VERBOSE:="false"}
LANGUAGE=`echo "$LANGUAGE" | tr [:upper:] [:lower:]`
COUNTER=1
MAX_RETRY=10

CC_SRC_PATH="github.com/chaincode/chaincode_example02/go/"
#CC_SRC_PATH="/opt/gopath/src/github.com/chaincode"
if [ "$LANGUAGE" = "node" ]; then
	CC_SRC_PATH="/opt/gopath/src/github.com/chaincode/"
fi

if [ "$LANGUAGE" = "java" ]; then
	CC_SRC_PATH="/opt/gopath/src/github.com/chaincode/chaincode_example02/java/"
fi

echo "Channel name : "$CHANNEL_NAME

# import utils
. scripts/utils.sh

createChannel() {

	setGlobals 0 ibm

	if [ -z "$CORE_PEER_TLS_ENABLED" -o "$CORE_PEER_TLS_ENABLED" = "false" ]; then
                set -x
		peer channel create -o orderer.abdo.com:7050 -c $CHANNEL_NAME -f ./channel-artifacts/channel.tx >&log.txt
		res=$?
                set +x
	else
				set -x
		peer channel create -o orderer.abdo.com:7050 -c $CHANNEL_NAME -f ./channel-artifacts/channel.tx --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA >&log.txt
		res=$?
				set +x
	fi
	cat log.txt
	verifyResult $res "Channel creation failed"
	echo "===================== Channel '$CHANNEL_NAME' created ===================== "
	echo
}

joinChannel () {
strings=(
    ibm
    hsbc
    qnb
)
	for i in "${strings[@]}"; do
			echo $i
	  for peer in 0 1; do
		joinChannelWithRetry $peer $i
		echo "===================== peer${peer}.${i} joined channel '$CHANNEL_NAME' ===================== "
		sleep $DELAY
		echo
	    done
	done
}

## Create channel
echo "Creating channel..."
createChannel

## Join all the peers to the channel
echo "Having all peers join the channel..."
joinChannel

## Set the anchor peers for each org in the channel
echo "Updating anchor peers for ibm..."
updateAnchorPeers 0 ibm
echo "Updating anchor peers for hsbc..."
updateAnchorPeers 0 hsbc
echo "Updating anchor peers for qnb..."
updateAnchorPeers 0 qnb

## Install chaincode on peer0.ibm, peer0.hsbc, peer0.qnb, peer1.ibm , peer1.hsbc, peer1.qnb
echo "Installing chaincode on peer0.ibm..."
installChaincode 0 ibm
#echo "Install chaincode on peer0.hsbc..."
#installChaincode 0 hsbc
#echo "Install chaincode on peer0.qnb..."
#installChaincode 0 qnb
#echo "Installing chaincode on peer1.ibm..."
#installChaincode 1 ibm
#echo "Install chaincode on peer1.hsbc..."
#installChaincode 1 hsbc
#echo "Install chaincode on peer1.qnb..."
#installChaincode 1 qnb

# Instantiate chaincode on peer0.ibm , peer0.hsbc, peer0.qnb, peer1.ibm , peer1.hsbc, peer1.qnb
echo "Instantiating chaincode on peer0.ibm..."
instantiateChaincode 0 ibm
#echo "Instantiating chaincode on peer0.hsbc..."
#instantiateChaincode 0 hsbc
#echo "Instantiating chaincode on peer0.qnb..."
#instantiateChaincode 0 qnb
#echo "Instantiating chaincode on peer1.ibm..."
#instantiateChaincode 1 ibm
#echo "Instantiating chaincode on peer1.hsbc..."
#instantiateChaincode 1 hsbc
#echo "Instantiating chaincode on peer1.qnb..."
#instantiateChaincode 1 qnb

# Query chaincode on peer0.org1
#echo "Querying chaincode on peer0.org1..."
#chaincodeQuery 0 1 100

# Invoke chaincode on peer0.org1 and peer0.org2
#echo "Sending invoke transaction on peer0.org1 peer0.org2..."
#chaincodeInvoke 0 1 0 2

## Install chaincode on peer1.org2
#echo "Installing chaincode on peer1.org2..."
#installChaincode 1 2

# Query on chaincode on peer1.org2, check if the result is 90
#echo "Querying chaincode on peer1.org2..."
#chaincodeQuery 1 2 90

echo
echo "========= All GOOD, BYFN execution completed =========== "
echo

echo
echo " _____   _   _   ____   "
echo "| ____| | \ | | |  _ \  "
echo "|  _|   |  \| | | | | | "
echo "| |___  | |\  | | |_| | "
echo "|_____| |_| \_| |____/  "
echo

exit 0
