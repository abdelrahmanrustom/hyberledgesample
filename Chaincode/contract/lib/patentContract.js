/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');

const patentStatus = {
    Default: {code: 1, text: 'New Patent'},
    Pending: {code: 2, text: 'Pending Publish Patent'},
    Published: {code: 3, text: 'Patent Published'},
    Rejected: {code: 4, text: 'Patent Rejected'}
};
class PatentContract extends Contract {


    async instantiate(ctx) {

        console.info('=========  Init From Patent Contract =========');

        // let emptyList = [];
        // await ctx.stub.putState('owners', Buffer.from(JSON.stringify(emptyList)));
        // await ctx.stub.putState('verfifiers', Buffer.from(JSON.stringify(emptyList)));
        // await ctx.stub.putState('publishers', Buffer.from(JSON.stringify(emptyList)));

        let owners = [];
        let owner = {
            id: '1',
            companyName: 'OwnerCompany',
            type: 'owner',
            patents: []
        };
        owners.push(owner.id);
        await ctx.stub.putState(owner.id, Buffer.from(JSON.stringify(owner)));
        owner = {
            id: '2',
            companyName: 'OwnerCompany2',
            type: 'owner',
            patents: []
        };
        await ctx.stub.putState(owner.id, Buffer.from(JSON.stringify(owner)));
        owners.push(owner.id);
        await ctx.stub.putState('owners', Buffer.from(JSON.stringify(owners)));

        let verifiers = [];
        let verifier = {
            id: '3',
            companyName: 'VerifierCompany',
            type: 'verifier',
            patents: []
        };
        verifiers.push(verifier.id);
        await ctx.stub.putState(verifier.id, Buffer.from(JSON.stringify(verifier)));
        verifier = {
            id: '4',
            companyName: 'VerifierCompany2',
            type: 'verifier',
            patents: []
        };
        verifiers.push(verifier.id);
        await ctx.stub.putState(verifier.id, Buffer.from(JSON.stringify(verifier)));
        await ctx.stub.putState('verifiers', Buffer.from(JSON.stringify(verifiers)));

        let publishers = [];
        let publisher = {
            id: '5',
            companyName: 'PublisherCompany',
            type: 'publisher',
            patents: []
        };
        publishers.push(publisher.id);
        await ctx.stub.putState(publisher.id, Buffer.from(JSON.stringify(publisher)));
        publisher = {
            id: '6',
            companyName: 'PublisherCompany2',
            type: 'publisher',
            patents: []
        };
        publishers.push(publisher.id);
        await ctx.stub.putState(publisher.id, Buffer.from(JSON.stringify(publisher)));
        await ctx.stub.putState('publishers', Buffer.from(JSON.stringify(publishers)));

        return "succes"
    }

    async query(ctx, key) {
        console.info('querying for key: ' + key  );
        let returnAsBytes = await ctx.stub.getState(key);
        let result = JSON.parse(returnAsBytes);
        return JSON.stringify(result);
    }


    //add a buyer object to the blockchain state identifited by the ownerId
    async RegisterOwner(ctx, ownerId, companyName) {

        let owner = {
            id: ownerId,
            companyName: companyName,
            type: 'owner',
            patents: []
        };
        await ctx.stub.putState(ownerId, Buffer.from(JSON.stringify(owner)));

        //add ownerId to 'owners' key
        let data = await ctx.stub.getState('owners');
        if (data) {
            let owners = JSON.parse(data.toString());
            owners.push(ownerId);
            await ctx.stub.putState('owners', Buffer.from(JSON.stringify(owners)));
        } else {
            throw new Error('owners not found');
        }

        // return owner object
        return JSON.stringify(owner);
    }

    // add a verifier object to the blockchain state identifited by the verifierId
    async RegisterVerifier(ctx, verifierId, companyName) {

        let verifier = {
            id: verifierId,
            companyName: companyName,
            type: 'verifier',
            patents: []
        };
        await ctx.stub.putState(verifierId, Buffer.from(JSON.stringify(verifier)));

        // add verifierId to 'verifiers' key
        let data = await ctx.stub.getState('verifiers');
        if (data) {
            let verifiers = JSON.parse(data.toString());
            verifiers.push(verifierId);
            await ctx.stub.putState('verifiers', Buffer.from(JSON.stringify(verifiers)));
        } else {
            throw new Error('verifiers not found');
        }

        // return verifier object
        return JSON.stringify(verifier);
    }


    // add a publisher object to the blockchain state identifited by the publisherId
    async RegisterPublisher(ctx, publisherId, companyName) {

        let publisher = {
            id: publisherId,
            companyName: companyName,
            type: 'publisher',
            patents: []
        };
        await ctx.stub.putState(publisherId, Buffer.from(JSON.stringify(publisher)));

        //add publisherId to 'publishers' key
        let data = await ctx.stub.getState('publishers');
        if (data) {
            let publishers = JSON.parse(data.toString());
            publishers.push(publisherId);
            await ctx.stub.putState('publishers', Buffer.from(JSON.stringify(publishers)));
        } else {
            throw new Error('publishers not found');
        }

        // return publisher object
        return JSON.stringify(publisher);
    }

    // // add an patent request object to the blockchain state identifited by the orderNumber
    async CreatePatentRequest(ctx, ownerId, verifierId, ownersId, patentNumber, patentIndustry, priorArt, details) {

        // verify ownerId
        let ownerData = await ctx.stub.getState(ownerId);
        let owner;
        if (ownerData) {
            owner = JSON.parse(ownerData.toString());
            if (owner.type !== 'owner') {
                throw new Error('owner not identified');
            }
        } else {
            throw new Error('owner not found');
        }
        
        // verify onwersId
        // let owners = []
        // ownersId.forEach(id => {
        //     let data = await ctx.stub.getState(id);
        //     let ownerData2;
        //     if (data) {
        //         ownerData2 = JSON.parse(data.toString())
        //         owners.push(ownerData2);
        //         if (ownerData2.type !== 'owner') {
        //             throw new Error('owner from ownersId not identified');
        //         }
        //     } else {
        //         throw new Error('owner from ownersId not found');
        //     }

        // });

        //verify verifierId
        let verifierData = await ctx.stub.getState(verifierId);
        let verifier;
        if (verifierData) {
            verifier = JSON.parse(verifierData.toString());
            if (verifier.type !== 'verifier') {
                throw new Error('verifier not identified');
            }
        } else {
            throw new Error('verifier not found');
        }

        let patent = {
            patentNumber: patentNumber,
            patentIndustry: patentIndustry,
            status: JSON.stringify(patentStatus.Default),
            priorArt: priorArt,
            details: details,
            ownersId: ownersId,
            ownerId: ownerId,
            verifierId: verifierId,
            publisherId: null,
            reason: null
        };
        //console.log(patent)

        //add patentRequest to verifierId
        verifier.patents.push(patentNumber);
        await ctx.stub.putState(verifierId, Buffer.from(JSON.stringify(verifier)));
        //console.log(verifier)
        // //add patentRequest to ownerId
        owner.patents.push(patentNumber);
        await ctx.stub.putState(ownerId, Buffer.from(JSON.stringify(owner)));
        //console.log(owner)

        // //store patent identified by patentNumber
        await ctx.stub.putState(patentNumber, Buffer.from(JSON.stringify(patent)));

        // return patent object
        return JSON.stringify(patent);
    }



    async VerifyPatent(ctx, patentNumber, verifierId, publisherId, action, reason) {

        // get patent json
        let data = await ctx.stub.getState(patentNumber);
        let patent;
        if (data) {
            patent = JSON.parse(data.toString());
        } else {
            throw new Error('patent not found');
        }

        // verify publisherId
        let publisherData = await ctx.stub.getState(publisherId);
        let publisher;
        if (publisherData) {
            publisher = JSON.parse(publisherData.toString());
            if (publisher.type !== 'publisher') {
                throw new Error('publisher not identified');
            }
        } else {
            throw new Error('publisher not found');
        }

         //verify verifierId
         let verifierData = await ctx.stub.getState(verifierId);
         let verifier;
         if (verifierData) {
             verifier = JSON.parse(verifierData.toString());
             if (verifier.type !== 'verifier') {
                 throw new Error('verifier not identified');
             }
         } else {
             throw new Error('verifier not found');
         } 
         let foundPatent = -1;
         verifier.patents.forEach(patentItem => {
             if(patentItem === patentNumber) {
                foundPatent = 1;
             }
         });

         if(foundPatent === -1) {
            throw new Error('Patent not assigned to this verifier');
         }

        // update patent status
        if (patent.status == JSON.stringify(patentStatus.Default)) {
            if(action === 'Pending'){
                patent.status = JSON.stringify(patentStatus.Pending);
            } else {
                patent.status = JSON.stringify(patentStatus.Rejected);
                patent.reason = reason;
            }
            patent.publisherId = publisherId;
            await ctx.stub.putState(patentNumber, Buffer.from(JSON.stringify(patent)));
            
            //add patent to publisherId
            publisher.patents.push(patentNumber);
            await ctx.stub.putState(publisherId, Buffer.from(JSON.stringify(publisher)));

            return JSON.stringify(patent);
        } else {
            throw new Error('patent is not new');
        }
    }


    async PublishPatent(ctx, patentNumber, publisherId) {
        // get patent json
        let data = await ctx.stub.getState(patentNumber);
        let patent;
        if (data) {
            patent = JSON.parse(data.toString());
        } else {
            throw new Error('patent not found');
        }

        // verify publisherId
        let publisherData = await ctx.stub.getState(publisherId);
        let publisher;
        if (publisherData) {
            publisher = JSON.parse(publisherData.toString());
            if (publisher.type !== 'publisher') {
                throw new Error('publisher not identified');
            }
        } else {
            throw new Error('publisher not found');
        }

        let foundPatent = -1;
        publisher.patents.forEach(patentItem => {
            if(patentItem === patentNumber) {
               foundPatent = 1;
            }
        });

        if(foundPatent === -1) {
           throw new Error('Patent not assigned to this publisher');
        }

        if (patent.status == JSON.stringify(patentStatus.Pending)) {
            patent.status = JSON.stringify(patentStatus.Published);
            await ctx.stub.putState(patentNumber, Buffer.from(JSON.stringify(patent)));
            return JSON.stringify(patent);
        } else {
            throw new Error('patent is not pending');
        }
    }


}

module.exports = PatentContract;
